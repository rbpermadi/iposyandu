import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose} from 'redux';
import registerServiceWorker from './registerServiceWorker';

import rootReducer from './rootReducer';
import setAuthorizationToken from './utils/setAuthorizationToken';
import routes from './routes';
import { setCurrentUser } from './actions/authActions';

const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(thunk),
		window.devToolsExtension ? window.devToolsExtension() : f => f
	)
);

if(localStorage.auth_token){
	setAuthorizationToken(localStorage.auth_token);
	store.dispatch(setCurrentUser({token:localStorage.auth_token}));
}

ReactDOM.render(
	<Provider store={store}>
		<Router history={ browserHistory } routes={routes} />
	</Provider>, document.getElementById('root'));
registerServiceWorker();
