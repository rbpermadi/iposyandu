import React from 'react';
import {Link, browserHistory} from 'react-router';
import classnames from 'classnames';

import validateInput from '../../utils/validations/signin';

class SignInForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            phone_number: '',
            password:'',
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    isValid() {
        const { errors, isValid } = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }

        return isValid;
    }

    onChange(e){
        this.setState( {[e.target.name]: e.target.value });
    }

    onSubmit(e){
        e.preventDefault();
        if (this.isValid()) {
            this.setState({ errors: {}, isLoading: true });
            this.props.login(this.state).then(
                (data) => {
                    if(data.response === "success"){
                        alert("You are Loged In");
                        browserHistory.push('/');
                    }
                    else{
                        if(data.hasOwnProperty('result') && data.result.hasOwnProperty("error")){
                            const error = data.result.error;
                            error.form = data.message;
                            this.setState({errors: error, isLoading:false});
                        }
                        else{
                            const error = {};
                            error.form = data.message;
                            this.setState({errors: error, isLoading:false});
                        }
                    }
                }
            );
        }
    }

	render(){
        const {phone_number, password, errors, isLoading} = this.state;
		return(
            <form onSubmit={this.onSubmit} >
                { errors.form && <div className="alert alert-danger">{errors.form}</div> }

                <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
                    <label className="control-label">No Handphone</label>
                    <input
                        value={phone_number || ''}
                        onChange={this.onChange}
                        type="text"
                        name="phone_number"
                        className="form-control"
                    />
                    {errors.phone_number && <span className="help-block">{errors.phone_number}</span>}
                </div>
                <div className={classnames('form-group', { 'has-error': errors.password })}>
                    <label className="control-label">Password</label>
                    <input
                        value={password || ''}
                        onChange={this.onChange}
                        type="password"
                        name="password"
                        className="form-control"
                    />
                    {errors.password && <span className="help-block">{errors.password}</span>}
                </div>
                <div className="form-group">
                    <button className="btn btn-primary btn-lg col-md-12" disabled={isLoading}>
                        SignIn
                    </button>
                </div>
                <div className="form-group">
                    <center>
                    Belum menjadi anggota? <Link to="/signup">Daftar</Link>    
                    </center>
                </div>
            </form>
		);
	}
}

SignInForm.propTypes = {
    login: React.PropTypes.func.isRequired
}

export default SignInForm;
