import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import { login } from '../../actions/authActions';
import SignInForm from './SignInForm';

class SignInPage extends React.Component {
	componentDidMount(){
		const { isAuthenticated } = this.props.auth;

		if (isAuthenticated) {
	      browserHistory.push('/');
	    }
	}

	render(){
    	const { login } = this.props;
		return(
			<div className="row">
				<div className="col-md-4 col-md-offset-4">
					<SignInForm login={login}/>
				</div>
			</div>
		);
	}
}

SignInPage.propTypes = {
    login: React.PropTypes.func.isRequired
}

function mapStateToProps(state){
	return {
		auth: state.auth
	};
}

export default connect( mapStateToProps, { login })(SignInPage);
