import React from 'react';
import { connect } from 'react-redux';

import { logout } from '../../actions/authActions';

class SignOutPage extends React.Component{
	componentDidMount(){
		this.props.logout();
	}
	render(){
		return (
			<div className="jumbotron">
				<h3>Loading</h3>
			</div>
		);
	}
}

SignOutPage.propTypes = {
	logout: React.PropTypes.func.isRequired
}

export default connect(null, { logout })(SignOutPage);

