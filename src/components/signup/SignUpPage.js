import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import { userSignupRequest } from '../../actions/signupActions';
import SignUpForm from './SignUpForm';

class SignUpPage extends React.Component {
	componentDidMount(){
		const { isAuthenticated } = this.props.auth;

		if (isAuthenticated) {
	      browserHistory.push('/');
	    }
	}

	render(){
		const { userSignupRequest } = this.props;
		return(
			<div className="row">
				<div className="col-md-4 col-md-offset-4">
					<SignUpForm userSignupRequest={userSignupRequest} />
				</div>
			</div>
		);
	}
}

SignUpPage.propTypes = {
    userSignupRequest: React.PropTypes.func.isRequired
}

function mapStateToProps(state){
	return {
		auth: state.auth
	};
}

export default connect( mapStateToProps, { userSignupRequest })(SignUpPage);