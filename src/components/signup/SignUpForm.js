import React from 'react';
import {Link, browserHistory} from 'react-router';
import classnames from 'classnames';

import validateInput from '../../utils/validations/signup';

class SignUpForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            public_healths_id:'12',
            role: 'posyandu',
            posyandus_id:'',
            phone_number: '',
            name:'',
            password:'',
            password_confirmation:'',
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    isValid() {
        const { errors, isValid } = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }

        return isValid;
    }

    onChange(e){
        this.setState( {[e.target.name]: e.target.value });
    }

    onSubmit(e){
        e.preventDefault();
        if (this.isValid()) {
            this.setState({ errors: {}, isLoading: true });
            this.props.userSignupRequest(this.state).then(
                (data) => {
                    if(data.response === "success"){
                        alert("Your registration success");
                        browserHistory.push('/');
                    }
                    else{
                        if(data.hasOwnProperty('result') && data.result.hasOwnProperty("error")){
                            const error = data.result.error;
                            error.form = data.message;
                            this.setState({errors: error, isLoading:false});
                        }
                        else{
                            const error = {};
                            error.form = data.message;
                            this.setState({errors: error, isLoading:false});
                        }
                    }
                }
            );
        }
    }

    render(){
        const {posyandus_id, phone_number, name, password, password_confirmation, errors, isLoading} = this.state;
        return(
            <form onSubmit={this.onSubmit} >
                {errors.form && <div className="has-error bg-danger"><h4>{errors.form}</h4></div>}
                <div className={classnames('form-group', { 'has-error': errors.posyandus_id })}>
                    <label className="control-label">Posyandu</label>
                    <select
                        value={posyandus_id || ''}
                        onChange={this.onChange}
                        type="text"
                        name="posyandus_id"
                        className="form-control"
                    >
                        <option value='' disabled>Pilih Posyandu</option>
                        <option value="1">Manggis I</option>
                        <option value="2">Manggis II</option>
                        <option value="3">Manggis III</option>
                        <option value="4">Manggis IV</option>
                    </select>
                    {errors.posyandus_id && <span className="help-block">{errors.posyandus_id}</span>}
                </div>
                <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
                    <label className="control-label">No Handphone</label>
                    <input
                        value={phone_number || ''}
                        onChange={this.onChange}
                        type="text"
                        name="phone_number"
                        className="form-control"
                    />
                    {errors.phone_number && <span className="help-block">{errors.phone_number}</span>}
                </div>
                <div className={classnames('form-group', { 'has-error': errors.name })}>
                    <label className="control-label">Nama</label>
                    <input
                        value={name || ''}
                        onChange={this.onChange}
                        type="text"
                        name="name"
                        className="form-control"
                    />
                    {errors.name && <span className="help-block">{errors.name}</span>}
                </div>
                <div className={classnames('form-group', { 'has-error': errors.password })}>
                    <label className="control-label">Password</label>
                    <input
                        value={password || ''}
                        onChange={this.onChange}
                        type="password"
                        name="password"
                        className="form-control"
                    />
                    {errors.password && <span className="help-block">{errors.password}</span>}
                </div>
                <div className={classnames('form-group', { 'has-error': errors.password_confirmation })}>
                    <label className="control-label">Password Confirmation</label>
                    <input
                        value={password_confirmation || ''}
                        onChange={this.onChange}
                        type="password"
                        name="password_confirmation"
                        className="form-control"
                    />
                    {errors.password_confirmation && <span className="help-block">{errors.password_confirmation}</span>}
                </div>
                <div className="form-group">
                    <button className="btn btn-primary btn-lg col-md-12" disabled={isLoading}>
                        SignUp!
                    </button>
                </div>
                <div className="form-group">
                    <center>
                    Sudah menjadi anggota? <Link to="/signin">Masuk</Link>    
                    </center>
                </div>
            </form>
        );
    }
}

SignUpForm.propTypes = {
    userSignupRequest: React.PropTypes.func.isRequired
}

export default SignUpForm;
