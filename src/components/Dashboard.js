import React from 'react';
import { connect } from 'react-redux';

import UserDashboard from './UserDashboard';
import { babyCount } from '../actions/babyActions';
import { expectantCount } from '../actions/expectantActions';

class Dashboard extends React.Component {
	constructor(props){
        super(props);
        this.state = {
            babyCount: "0",
            expectantCount: "0"
        }
    }

	componentDidMount(){
		const { isAuthenticated } = this.props.auth;

		if(isAuthenticated){
			this.props.babyCount().then(
				(data) => {
                    if(data.response === "success"){
                        this.setState({babyCount: data.result.total_baby});
                    }
                }
			)

			this.props.expectantCount().then(
				(data) => {
                    if(data.response === "success"){
                        this.setState({expectantCount: data.result.total_expectant});
                    }
                }
			)
		}		
	}

	render(){

		const { isAuthenticated } = this.props.auth;
		const GuestPage = (
			<div className="jumbotron">
                <h1>Welcome to iPosyandu Application</h1>
            </div>
		);

		const UserPage = (
            <UserDashboard data={this.state} />
		);

		return (
			<div>
				{isAuthenticated ? UserPage : GuestPage}
			</div>
		);

	}
}

Dashboard.propTypes = {
	auth: React.PropTypes.object.isRequired,
	babyCount: React.PropTypes.func.isRequired,
	expectantCount: React.PropTypes.func.isRequired
}

function mapStateToProps(state){
	return {
		auth: state.auth
	};
}

export default connect(mapStateToProps, {babyCount, expectantCount})(Dashboard);