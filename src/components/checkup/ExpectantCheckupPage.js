import React from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn, InsertButton, DeleteButton}  from 'react-bootstrap-table';

import { getExpectantCheckup, deleteExpectantCheckup } from '../../actions/expectantCheckupActions';

class ExpectantCheckupPage extends React.Component {
	constructor(props){
        super(props);
        this.state = {
            expectants: []
        }
    }

	componentDidMount(){
		this.props.getExpectantCheckup().then(
			(data) => {
                if(data.response === "success"){
                	const expectants = data.result.data;
                	for (var i = 0; i < expectants.length; i++) {
					  expectants[i].edit = expectants[i].id;
					  expectants[i].ttd = (expectants[i].ttd === 0) ? "tidak" : ((expectants[i].ttd === 1) ? "ya" : null);
					  expectants[i].kia = (expectants[i].kia === 0) ? "tidak" : ((expectants[i].kia === 1) ? "ya" : null);
					}
                    this.setState({expectants: expectants});
                }
            }
		);
	}

	editFormatter(cell, row) {
	  return `<a href='expectant/edit/${cell}' class='btn btn-warning'>edit</a> `;
	}

	handleInsertButtonClick = (onClick) => {
	  // Custom your onClick event here,
	  // it's not necessary to implement this function if you have no any process before onClick
	  browserHistory.push('/checkup/expectant/add');
	}

	createCustomInsertButton = (onClick) => {
	  return (
	    <InsertButton
	      btnText='Add'
	      btnContextual='btn-warning'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-edit'
	      onClick={ () => this.handleInsertButtonClick(onClick) }/>
	  );
	}

	createCustomDeleteButton = (onClick) => {
	  return (
	    <DeleteButton
	      btnText='Delete'
	      btnContextual='btn-danger'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-trash'/>
	  );
	}

	handleDeleteRow(row) {
		this.props.deleteExpectantCheckup(row).then(
			(data) => {
				this.componentDidMount();			
			}
		);
	}

	render(){
		const expectants = this.state.expectants;
		const options = {
			insertBtn: this.createCustomInsertButton,
			onDeleteRow: this.handleDeleteRow.bind(this),
			deleteBtn: this.createCustomDeleteButton
		};
		const selectRow = {
		    mode: 'radio'
		}

		const remote = (remoteFunction) => {
		    // remoteFunction is a object, default is all false, 
		    //  { insertRow: false, dropRow: false, cellEdit: false, sort: false, pagination: false, search: false, filter: false    }
		    //
		    remoteFunction.insertRow = true;
		    remoteFunction.dropRow = true;
		    remoteFunction.cellEdit = true;
		    return remoteFunction;
		}

		return(

			<div>
				<div className="col-md-12">
					<Link type="button" to="/checkup/baby" className="btn btn-default btn-lg col-md-6">Bayi</Link>
					<Link type="button" to="/checkup/expectant" className="btn btn-primary btn-lg col-md-6">Ibu Hamil</Link>
				</div>
				<div>&nbsp;</div>
				<BootstrapTable data={expectants} options={options} remote={ remote } deleteRow={ true } selectRow= { selectRow } striped hover search multiColumnSearch pagination insertRow>
					<TableHeaderColumn isKey dataField='id'>Id</TableHeaderColumn>
					<TableHeaderColumn dataField='checkup_date'>Tanggal Checkup</TableHeaderColumn>
					<TableHeaderColumn dataField='nik'>Nik</TableHeaderColumn>
					<TableHeaderColumn dataField='name'>Nama</TableHeaderColumn>
					<TableHeaderColumn dataField='weight_before_pregnant'>Berat Sebelum Hamil</TableHeaderColumn>
					<TableHeaderColumn dataField='height'>Tinggi Badan</TableHeaderColumn>
					<TableHeaderColumn dataField='weight'>Berat Badan</TableHeaderColumn>
					<TableHeaderColumn dataField='pregnancy_age'>Umur Kehamilan</TableHeaderColumn>
					<TableHeaderColumn dataField='ula'>ULA</TableHeaderColumn>
					<TableHeaderColumn dataField='hb'>HB</TableHeaderColumn>
					<TableHeaderColumn dataField='gpa'>GPA</TableHeaderColumn>
					<TableHeaderColumn dataField='kia'>KIA</TableHeaderColumn>
					<TableHeaderColumn dataField='ttd'>TTD</TableHeaderColumn>
					<TableHeaderColumn dataField='ttd_amount'>Jumlah TTD</TableHeaderColumn>
					<TableHeaderColumn dataField="edit" dataFormat={this.editFormatter}>Buttons</TableHeaderColumn>
				</BootstrapTable>
			</div>	
					
		);
	}
}

ExpectantCheckupPage.propTypes = {
	getExpectantCheckup: React.PropTypes.func.isRequired,
	deleteExpectantCheckup: React.PropTypes.func.isRequired
}

export default connect(null, {getExpectantCheckup, deleteExpectantCheckup})(ExpectantCheckupPage);
