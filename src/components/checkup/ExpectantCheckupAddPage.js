import React from 'react';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import map from 'lodash/map';
 
import 'react-datepicker/dist/react-datepicker.css';

import { addExpectantCheckup } from '../../actions/expectantCheckupActions';
import { getExpectant } from '../../actions/expectantActions';

class ExpectantCheckupAddPage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
        	posyandus_id: localStorage.posyandus_id,
            expectants: [],
        	expectants_id:'',
        	date: '',
            checkup_date: '',
            height: '',
            weight: '',
            ula: '',
            hb: '',
            gpa: '',
            pregnancy_age: '',
            kia: null,
            ttd: null,
            ttd_amount: null,
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    onChange(e){
        this.setState( {[e.target.name]: e.target.value });
    }

    onSubmit(e){
    	this.setState({ errors: {}, isLoading: true });
        e.preventDefault();
        const param = this.state;
        const date = param.date;
        const expectants = param.expectants;
        delete param.errors;
        delete param.isLoading;
        delete param.date;
        delete param.expectants;
        param.checkup_date = (date !== '') ? date.format("YYYY-MM-DD") : null;
        this.props.addExpectantCheckup(param).then(
			(data) => {
                if(data.response === "success"){
                	alert(data.message);
                    browserHistory.push('/checkup/expectant');
                }
                else{
                	this.setState({expectants: expectants});
                	this.setState({date: date});
                    if(data.hasOwnProperty('result') && data.result.hasOwnProperty("error")){
                        const error = data.result.error;
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                    else{
                        const error = {};
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                }
            }
		);
    }

    componentDidMount(){
		this.props.getExpectant().then(
			(data) => {
                if(data.response === "success"){
                	const expectants = data.result.data;
                    this.setState( {
                    	expectants:  expectants
                     });
                 }
            }
		);		
	}

	handleSelect(date) {
		this.setState({
			date: date
		});
	}
	
	render(){
		const {errors, isLoading, expectants} = this.state;
		const expectants_options = map(expectants, (expectant, key) => 
			<option key={expectant.id} value={expectant.id}>{expectant.name}</option>
		);

		return(
			<div className="row">
				<div className="col-md-4 col-md-offset-4">
					<form onSubmit={this.onSubmit} >
						{ errors.form && <div className="alert alert-danger">{errors.form}</div> }
						<div className={classnames('form-group', { 'has-error': errors.checkup_date })}>
		                    <label className="control-label">Tanggal Checkup</label>
		                    <div className="form-control">
			                    <DatePicker showYearDropdown selected={this.state.date} onChange={this.handleSelect} />
							</div>
		                    
		                    {errors.checkup_date && <span className="help-block">{errors.checkup_date}</span>}
		                </div>
						<div className={classnames('form-group', { 'has-error': errors.expectants_id })}>
		                    <label className="control-label">Ibu Hamil</label>
		                    <select
		                        value={this.state.expectants_id}
		                        onChange={this.onChange}
		                        type="text"
		                        name="expectants_id"
		                        className="form-control"
		                    >
		                        <option value='' disabled>Pilih Ibu Hamil</option>
		                        {expectants_options}
		                    </select>
		                    {errors.expectants_id && <span className="help-block">{errors.expectants_id}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.height })}>
		                    <label className="control-label">Tinggi Badan</label>
		                    <input
		                        value={this.state.height || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="height"
		                        className="form-control"
		                    />
		                    {errors.height && <span className="help-block">{errors.height}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.weight })}>
		                    <label className="control-label">Berat Badan</label>
		                    <input
		                        value={this.state.weight || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="weight"
		                        className="form-control"
		                    />
		                    {errors.weight && <span className="help-block">{errors.weight}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.ula })}>
		                    <label className="control-label">ULA</label>
		                    <input
		                        value={this.state.ula || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="ula"
		                        className="form-control"
		                    />
		                    {errors.ula && <span className="help-block">{errors.ula}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.hb })}>
		                    <label className="control-label">HB</label>
		                    <input
		                        value={this.state.hb || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="hb"
		                        className="form-control"
		                    />
		                    {errors.hb && <span className="help-block">{errors.hb}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.gpa })}>
		                    <label className="control-label">GPA</label>
		                    <input
		                        value={this.state.gpa || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="gpa"
		                        className="form-control"
		                    />
		                    {errors.gpa && <span className="help-block">{errors.gpa}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.pregnancy_age })}>
		                    <label className="control-label">Umur Kehamilan</label>
		                    <input
		                        value={this.state.pregnancy_age || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="pregnancy_age"
		                        className="form-control"
		                    />
		                    {errors.pregnancy_age && <span className="help-block">{errors.pregnancy_age}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.kia })}>
		                    <label className="control-label">KIA</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="kia" value='0' checked={this.state.kia === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="kia" value='1' checked={this.state.kia === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.kia && <span className="help-block">{errors.kia}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.ttd })}>
		                    <label className="control-label">TTD</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="ttd" value='0' checked={this.state.ttd === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="ttd" value='1' checked={this.state.ttd === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.ttd && <span className="help-block">{errors.ttd}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.ttd_amount })}>
		                    <label className="control-label">Total TTD</label>
		                    <input
		                        value={this.state.ttd_amount || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="ttd_amount"
		                        className="form-control"
		                    />
		                    {errors.ttd && <span className="help-block">{errors.ttd_amount}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
		                    <button className="btn btn-primary btn-lg col-md-12" disabled={isLoading}>
		                        Save!
		                    </button>
		                </div>
		            </form>
				</div>
			</div>
		            
		);
	}
}

ExpectantCheckupAddPage.propTypes = {
	addExpectantCheckup: React.PropTypes.func.isRequired,
	getExpectant: React.PropTypes.func.isRequired
}

export default connect(null, {getExpectant, addExpectantCheckup})(ExpectantCheckupAddPage);
