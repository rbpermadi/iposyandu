import React from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn, InsertButton, DeleteButton}  from 'react-bootstrap-table';

import { getBabyCheckup, deleteBabyCheckup } from '../../actions/babyCheckupActions';

class BabyCheckupPage extends React.Component {
	constructor(props){
        super(props);
        this.state = {
            babies: []
        }
    }

	componentDidMount(){
		this.props.getBabyCheckup().then(
			(data) => {
                if(data.response === "success"){
                	const babies = data.result.data;
                	for (var i = 0; i < babies.length; i++) {
					  babies[i].edit = babies[i].id;
					  babies[i].exclusive_breast_milk = (babies[i].exclusive_breast_milk === 0) ? "tidak" : ((babies[i].exclusive_breast_milk === 1) ? "ya" : null);
					  babies[i].imd = (babies[i].imd === 0) ? "tidak" : ((babies[i].imd === 1) ? "ya" : null);
					  babies[i].aug_vit_a = (babies[i].aug_vit_a === 0) ? "tidak" : ((babies[i].aug_vit_a === 1) ? "ya" : null);
					  babies[i].feb_vit_a = (babies[i].feb_vit_a === 0) ? "tidak" : ((babies[i].feb_vit_a === 1) ? "ya" : null);
					  babies[i].kia = (babies[i].kia === 0) ? "tidak" : ((babies[i].kia === 1) ? "ya" : null);
					}
                    this.setState({babies: babies});
                }
            }
		);
	}

	editFormatter(cell, row) {
	  return `<a href='baby/edit/${cell}' class='btn btn-warning'>edit</a> `;
	}

	handleInsertButtonClick = (onClick) => {
	  // Custom your onClick event here,
	  // it's not necessary to implement this function if you have no any process before onClick
	  browserHistory.push('/checkup/baby/add');
	}

	createCustomInsertButton = (onClick) => {
	  return (
	    <InsertButton
	      btnText='Add'
	      btnContextual='btn-warning'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-edit'
	      onClick={ () => this.handleInsertButtonClick(onClick) }/>
	  );
	}

	createCustomDeleteButton = (onClick) => {
	  return (
	    <DeleteButton
	      btnText='Delete'
	      btnContextual='btn-danger'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-trash'/>
	  );
	}

	handleDeleteRow(row) {
		this.props.deleteBabyCheckup(row).then(
			(data) => {
				this.componentDidMount();			
			}
		);
	}

	render(){
		const babies = this.state.babies;
		const options = {
			insertBtn: this.createCustomInsertButton,
			onDeleteRow: this.handleDeleteRow.bind(this),
			deleteBtn: this.createCustomDeleteButton
		};
		const selectRow = {
		    mode: 'radio'
		}

		const remote = (remoteFunction) => {
		    // remoteFunction is a object, default is all false, 
		    //  { insertRow: false, dropRow: false, cellEdit: false, sort: false, pagination: false, search: false, filter: false    }
		    //
		    remoteFunction.insertRow = true;
		    remoteFunction.dropRow = true;
		    remoteFunction.cellEdit = true;
		    return remoteFunction;
		}
			
		return(
			<div>
				<div className="col-md-12">
					<Link type="button" to="/checkup/baby" className="btn btn-primary btn-default btn-lg col-md-6">Bayi</Link>
					<Link type="button" to="/checkup/expectant" className="btn btn-default btn-lg col-md-6">Ibu Hamil</Link>
				</div>
				<div>&nbsp;</div>
				<BootstrapTable data={babies} options={options} remote={remote} deleteRow={ true } selectRow= { selectRow } striped hover search multiColumnSearch pagination insertRow>
					<TableHeaderColumn isKey dataField='id'>Id</TableHeaderColumn>
					<TableHeaderColumn dataField='checkup_date'>Tanggal Checkup</TableHeaderColumn>
					<TableHeaderColumn dataField='nik'>Nik</TableHeaderColumn>
					<TableHeaderColumn dataField='name'>Nama</TableHeaderColumn>
					<TableHeaderColumn dataField='birth_weight'>Berat Saat Lahir</TableHeaderColumn>
					<TableHeaderColumn dataField='height'>Tinggi Badan</TableHeaderColumn>
					<TableHeaderColumn dataField='weight'>Berat Badan</TableHeaderColumn>
					<TableHeaderColumn dataField='measure'>Cara Ukur</TableHeaderColumn>
					<TableHeaderColumn dataField='exclusive_breast_milk'>ASI</TableHeaderColumn>
					<TableHeaderColumn dataField='imd'>IMD</TableHeaderColumn>
					<TableHeaderColumn dataField='feb_vit_a'>Februari Vit A</TableHeaderColumn>
					<TableHeaderColumn dataField='aug_vit_a'>Agustus Vit A</TableHeaderColumn>
					<TableHeaderColumn dataField='kia'>KIA</TableHeaderColumn>
					<TableHeaderColumn dataField="edit" dataFormat={this.editFormatter}>Buttons</TableHeaderColumn>
				</BootstrapTable>
			</div>	
		);
	}
}

BabyCheckupPage.propTypes = {
	getBabyCheckup: React.PropTypes.func.isRequired,
	deleteBabyCheckup: React.PropTypes.func.isRequired
}

export default connect(null, {getBabyCheckup, deleteBabyCheckup})(BabyCheckupPage);
