import React from 'react';

class CheckupMainPage extends React.Component {
	render() {
		return (
			<div>
				{this.props.children}
			</div>
		);
	}
}

export default CheckupMainPage;