import React from 'react';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import map from 'lodash/map';
 
import 'react-datepicker/dist/react-datepicker.css';

import { addBabyCheckup } from '../../actions/babyCheckupActions';
import { getBaby } from '../../actions/babyActions';

class BabyCheckupAddPage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
        	posyandus_id: localStorage.posyandus_id,
        	babies: [],
        	babies_id:'',
        	date: '',
            checkup_date: '',
            height: '',
            weight: '',
            measure: null,
            exclusive_breast_milk: '',
            imd: '',
            feb_vit_a: '',
            aug_vit_a: '',
            kia: '',
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    onChange(e){
        this.setState( {[e.target.name]: e.target.value });
    }

    onSubmit(e){
    	this.setState({ errors: {}, isLoading: true });
        e.preventDefault();
        const param = this.state;
        const date = param.date;
        const babies = param.babies;
        delete param.errors;
        delete param.isLoading;
        delete param.date;
        delete param.babies;
        param.checkup_date = (date !== '') ? date.format("YYYY-MM-DD") : null;
        this.props.addBabyCheckup(param).then(
			(data) => {
                if(data.response === "success"){
                	alert(data.message);
                    browserHistory.push('/checkup/baby');
                }
                else{
                	this.setState({babies: babies});
                	this.setState({date: date});
                    if(data.hasOwnProperty('result') && data.result.hasOwnProperty("error")){
                        const error = data.result.error;
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                    else{
                        const error = {};
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                }
            }
		);
    }

    componentDidMount(){
		this.props.getBaby().then(
			(data) => {
                if(data.response === "success"){
                	const babies = data.result.data;
                    this.setState( {
                    	babies:  babies
                     });
                 }
            }
		);		
	}

	handleSelect(date) {
		this.setState({
			date: date
		});
	}
	
	render(){
		const {errors, isLoading, babies} = this.state;
		const babies_options = map(babies, (baby, key) => 
			<option key={baby.id} value={baby.id}>{baby.name}</option>
		);

		return(
			<div className="row">
				<div className="col-md-4 col-md-offset-4">
					<form onSubmit={this.onSubmit} >
						{ errors.form && <div className="alert alert-danger">{errors.form}</div> }
						<div className={classnames('form-group', { 'has-error': errors.checkup_date })}>
		                    <label className="control-label">Tanggal Checkup</label>
		                    <div className="form-control">
			                    <DatePicker showYearDropdown selected={this.state.date} onChange={this.handleSelect} />
							</div>
		                    
		                    {errors.checkup_date && <span className="help-block">{errors.checkup_date}</span>}
		                </div>
						<div className={classnames('form-group', { 'has-error': errors.babies_id })}>
		                    <label className="control-label">Bayi</label>
		                    <select
		                        value={this.state.babies_id}
		                        onChange={this.onChange}
		                        type="text"
		                        name="babies_id"
		                        className="form-control"
		                    >
		                        <option value='' disabled>Pilih Bayi</option>
		                        {babies_options}
		                    </select>
		                    {errors.babies_id && <span className="help-block">{errors.babies_id}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.height })}>
		                    <label className="control-label">Tinggi Badan</label>
		                    <input
		                        value={this.state.height || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="height"
		                        className="form-control"
		                    />
		                    {errors.height && <span className="help-block">{errors.height}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.weight })}>
		                    <label className="control-label">Berat Badan</label>
		                    <input
		                        value={this.state.weight || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="weight"
		                        className="form-control"
		                    />
		                    {errors.weight && <span className="help-block">{errors.weight}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.measure })}>
		                    <label className="control-label">Cara Ukur</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="measure" value="telentang" checked={this.state.measure === 'telentang'} onChange={this.onChange}/> Telentang
								</label>
								<label className="radio-inline">
								  <input type="radio" name="gender" value="berdiri" checked={this.state.measure === "berdiri"} onChange={this.onChange}/> Berdiri
								</label>
							</div>
							{errors.measure && <span className="help-block">{errors.measure}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.exclusive_breast_milk })}>
		                    <label className="control-label">Asi Ekslusif</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="exclusive_breast_milk" value='0' checked={this.state.exclusive_breast_milk === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="exclusive_breast_milk" value='1' checked={this.state.exclusive_breast_milk === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.exclusive_breast_milk && <span className="help-block">{errors.exclusive_breast_milk}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.imd })}>
		                    <label className="control-label">Imd</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="imd" value='0' checked={this.state.imd === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="imd" value='1' checked={this.state.imd === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.imd && <span className="help-block">{errors.imd}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.feb_vit_a })}>
		                    <label className="control-label">Februari Vit A</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="feb_vit_a" value='0' checked={this.state.feb_vit_a === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="feb_vit_a" value='1' checked={this.state.feb_vit_a === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.feb_vit_a && <span className="help-block">{errors.feb_vit_a}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.aug_vit_a })}>
		                    <label className="control-label">Agustus Vit A</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="aug_vit_a" value='0' checked={this.state.aug_vit_a === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="aug_vit_a" value='1' checked={this.state.aug_vit_a === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.aug_vit_a && <span className="help-block">{errors.aug_vit_a}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.kia })}>
		                    <label className="control-label">KIA</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="kia" value='0' checked={this.state.kia === '0'} onChange={this.onChange}/> Tidak
								</label>
								<label className="radio-inline">
								  <input type="radio" name="kia" value='1' checked={this.state.kia === '1'} onChange={this.onChange}/> Ya
								</label>
							</div>
							{errors.kia && <span className="help-block">{errors.kia}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
		                    <button className="btn btn-primary btn-lg col-md-12" disabled={isLoading}>
		                        Save!
		                    </button>
		                </div>
		            </form>
				</div>
			</div>
		            
		);
	}
}

BabyCheckupAddPage.propTypes = {
	addBabyCheckup: React.PropTypes.func.isRequired,
	getBaby: React.PropTypes.func.isRequired
}

export default connect(null, {getBaby, addBabyCheckup})(BabyCheckupAddPage);
