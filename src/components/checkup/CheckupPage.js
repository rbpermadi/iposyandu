import React from 'react';
import { browserHistory } from 'react-router';

class CheckupPage extends React.Component {
	componentDidMount(){
		if(!this.props.children){
			browserHistory.push('/checkup/baby');
		}
	}
	render() {
		return (
			<div className="jumbotron">
				<h3>Loading</h3>
			</div>
		);
	}
}

export default CheckupPage;