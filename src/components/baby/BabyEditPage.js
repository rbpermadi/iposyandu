import React from 'react';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import moment from 'moment';
 
import 'react-datepicker/dist/react-datepicker.css';

import { getBabyDetail, editBaby } from '../../actions/babyActions';

class BabyEditPage extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
        	posyandus_id: '',
            nik: '',
            name: '',
            child_no: '',
            date: '',
            gender: '',
            birth_weight: '',
            father_name: '',
            father_nik: '',
            mother_name: '',
            mother_nik: '',
            parent_phone: '',
            address: '',
            rt: '',
            rw: '',
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onChange(e){
        this.setState( {[e.target.name]: e.target.value });
    }

    onSubmit(e){
    	e.preventDefault();
    	this.setState({ errors: {}, isLoading: true });
        const param = this.state;
        const date = param.date;
        delete param.errors;
        delete param.isLoading;
        delete param.date;
        param.dob = (date !== '') ? date.format("YYYY-MM-DD") : null;
        this.props.editBaby(param, this.props.params.id).then(
			(data) => {
                if(data.response === "success"){
                	alert(data.message);
                    browserHistory.push('/baby');
                }
                else{
                	this.setState({date: date});
                    if(data.hasOwnProperty('result') && data.result.hasOwnProperty("error")){
                        const error = data.result.error;
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                    else{
                        const error = {};
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                }
            }
		);
    }

    handleChange(date) {
    	this.setState({
			date: date
		});
	}

    componentDidMount(){
		this.props.getBabyDetail(this.props.params.id).then(
			(data) => {
                if(data.response === "success"){
                	const baby = data.result.data;
                    this.setState( {
                    	posyandus_id:  baby.posyandus_id,
			            nik:  baby.nik,
			            name:  baby.name,
			            child_no:  baby.child_no,
			            date:  (baby.dob) ? moment(baby.dob, "YYYY-MM-DD"):'',
			            gender:  baby.gender,
			            birth_weight:  baby.birth_weight,
			            father_name:  baby.father_name,
			            father_nik:  baby.father_nik,
			            mother_name:  baby.mother_name,
			            mother_nik:  baby.mother_name,
			            parent_phone:  baby.parent_phone,
			            address:  baby.address,
			            rt:  baby.rt,
			            rw:  baby.rw,
                     });
                 }
            }
		);		
	}

	render(){
		const { errors, isLoading } = this.state;
		return(
			<div className="row">
				<div className="col-md-4 col-md-offset-4">
					<form onSubmit={this.onSubmit} >
						{ errors.form && <div className="alert alert-danger">{errors.form}</div> }
		                <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
		                    <label className="control-label">Nik</label>
		                    <input
		                        value={this.state.nik || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="nik"
		                        className="form-control"
		                    />
		                    {errors.nik && <span className="help-block">{errors.nik}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.name })}>
		                    <label className="control-label">Nama</label>
		                    <input
		                        value={this.state.name || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="name"
		                        className="form-control"
		                    />
		                    {errors.name && <span className="help-block">{errors.name}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.child_no })}>
		                    <label className="control-label">Anak Ke</label>
		                    <input
		                        value={this.state.child_no || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="child_no"
		                        className="form-control"
		                    />
		                    {errors.child_no && <span className="help-block">{errors.child_no}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.dob })}>
		                    <label className="control-label">Tanggal Lahir</label>
		                    <div className="form-control">
			                    <DatePicker showYearDropdown selected={this.state.date} onChange={this.handleChange} />
							</div>
		                    {errors.dob && <span className="help-block">{errors.dob}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.gender })}>
		                    <label className="control-label">Jenis Kelamin</label>
		                    <div className="form-control">
			                    <label className="radio-inline">
								  <input type="radio" name="gender" value="male" checked={this.state.gender === 'male'} onChange={this.onChange}/> Laki-Laki
								</label>
								<label className="radio-inline">
								  <input type="radio" name="gender" value="female" checked={this.state.gender === "female"} onChange={this.onChange}/> Perempuan
								</label>
							</div>
							{errors.gender && <span className="help-block">{errors.gender}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.birth_weight })}>
		                    <label className="control-label">Berat Saat Lahir</label>
		                    <input
		                        value={this.state.birth_weight || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="birth_weight"
		                        className="form-control"
		                    />
		                    {errors.nik && <span className="help-block">{errors.birth_weight}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.father_name })}>
		                    <label className="control-label">Nama Ayah</label>
		                    <input
		                        value={this.state.father_name || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="father_name"
		                        className="form-control"
		                    />
		                    {errors.father_name && <span className="help-block">{errors.father_name}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.father_nik })}>
		                    <label className="control-label">Nik Ayah</label>
		                    <input
		                        value={this.state.father_nik || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="father_nik"
		                        className="form-control"
		                    />
		                    {errors.father_nik && <span className="help-block">{errors.father_nik}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.mother_name })}>
		                    <label className="control-label">Nama Ibu</label>
		                    <input
		                        value={this.state.mother_name || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="mother_name"
		                        className="form-control"
		                    />
		                    {errors.mother_name && <span className="help-block">{errors.mother_name}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.mother_nik })}>
		                    <label className="control-label">Nik Ibu</label>
		                    <input
		                        value={this.state.mother_nik || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="mother_nik"
		                        className="form-control"
		                    />
		                    {errors.mother_nik && <span className="help-block">{errors.mother_nik}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.parent_phone })}>
		                    <label className="control-label">Telepon Orang Tua</label>
		                    <input
		                        value={this.state.parent_phone || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="parent_phone"
		                        className="form-control"
		                    />
		                    {errors.parent_phone && <span className="help-block">{errors.parent_phone}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.address })}>
		                    <label className="control-label">Alamat</label>
		                    <textarea 
		                    	value={this.state.address || ''}
		                        onChange={this.onChange}
		                    	className="form-control" 
		                    	name="address"
		                    	rows="3">
		                    </textarea>
		                    {errors.address && <span className="help-block">{errors.address}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.rt })}>
		                    <label className="control-label">Rt</label>
		                    <input
		                        value={this.state.rt || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="rt"
		                        className="form-control"
		                    />
		                    {errors.rt && <span className="help-block">{errors.rt}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.rw })}>
		                    <label className="control-label">Rw</label>
		                    <input
		                        value={this.state.rw || ''}
		                        onChange={this.onChange}
		                        type="text"
		                        name="rw"
		                        className="form-control"
		                    />
		                    {errors.rw && <span className="help-block">{errors.rw}</span>}
		                </div>
		                <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
		                    <button className="btn btn-primary btn-lg col-md-12" disabled={isLoading}>
		                        Save!
		                    </button>
		                </div>
		            </form>
				</div>
			</div>
		            
		);
	}
}

BabyEditPage.propTypes = {
	getBabyDetail: React.PropTypes.func.isRequired,
	editBaby: React.PropTypes.func.isRequired
}

export default connect(null, {getBabyDetail, editBaby})(BabyEditPage);
