import React from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn, InsertButton, DeleteButton}  from 'react-bootstrap-table';

import { getBaby, deleteBaby } from '../../actions/babyActions';

class BabyPage extends React.Component {
	constructor(props){
        super(props);
        this.state = {
            babies: []
        }
    }

	componentDidMount(){
		this.props.getBaby().then(
			(data) => {
                if(data.response === "success"){
                	const babies = data.result.data;
                	for (var i = 0; i < babies.length; i++) {
					  babies[i].edit = babies[i].id;
					  babies[i].address = ((babies[i].address) ? babies[i].address : "") + ((babies[i].rt) ? " RT " + babies[i].rt : "") + ((babies[i].rw) ? " RW " + babies[i].rw : "");
					}
                    this.setState({babies: babies});
                }
            }
		);
	}

	editFormatter(cell, row) {
	  return `<a href='baby/edit/${cell}' class='btn btn-warning'>edit</a> `;
	}

	handleInsertButtonClick = (onClick) => {
	  // Custom your onClick event here,
	  // it's not necessary to implement this function if you have no any process before onClick
	  browserHistory.push('/baby/add');
	}

	createCustomInsertButton = (onClick) => {
	  return (
	    <InsertButton
	      btnText='Add'
	      btnContextual='btn-warning'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-edit'
	      onClick={ () => this.handleInsertButtonClick(onClick) }/>
	  );
	}

	createCustomDeleteButton = (onClick) => {
	  return (
	    <DeleteButton
	      btnText='Delete'
	      btnContextual='btn-danger'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-trash'/>
	  );
	}

	handleDeleteRow(row) {
		this.props.deleteBaby(row).then(
			(data) => {
				this.componentDidMount();			
			}
		);
	}

	render(){
		const babies = this.state.babies;
		const options = {
			insertBtn: this.createCustomInsertButton,
			onDeleteRow: this.handleDeleteRow.bind(this),
			deleteBtn: this.createCustomDeleteButton
		};
		const selectRow = {
		    mode: 'radio'
		}

		const remote = (remoteFunction) => {
		    // remoteFunction is a object, default is all false, 
		    //  { insertRow: false, dropRow: false, cellEdit: false, sort: false, pagination: false, search: false, filter: false    }
		    //
		    remoteFunction.insertRow = true;
		    remoteFunction.dropRow = true;
		    remoteFunction.cellEdit = true;
		    return remoteFunction;
		}

		return(
			<BootstrapTable data={babies} options={options} remote={ remote } deleteRow={ true } selectRow= { selectRow } striped hover search multiColumnSearch pagination insertRow>
				<TableHeaderColumn isKey dataField='id'>Id</TableHeaderColumn>
				<TableHeaderColumn dataField='nik'>Nik</TableHeaderColumn>
				<TableHeaderColumn dataField='name'>Nama</TableHeaderColumn>
				<TableHeaderColumn dataField='child_no'>Anak Ke</TableHeaderColumn>
				<TableHeaderColumn dataField='dob'>Tanggal Lahir</TableHeaderColumn>
				<TableHeaderColumn dataField='gender'>Jenis Kelamin</TableHeaderColumn>
				<TableHeaderColumn dataField='birth_weight'>Berat saat Lahir</TableHeaderColumn>
				<TableHeaderColumn dataField='father_name'>Nama Ayah</TableHeaderColumn>
				<TableHeaderColumn dataField='father_nik'>Nik Ayah</TableHeaderColumn>
				<TableHeaderColumn dataField='mother_name'>Nama Ibu</TableHeaderColumn>
				<TableHeaderColumn dataField='mother_nik'>Nik Ibu</TableHeaderColumn>
				<TableHeaderColumn dataField='parent_phone'>Telepon Orang Tua</TableHeaderColumn>
				<TableHeaderColumn dataField='address'>Alamat</TableHeaderColumn>
				<TableHeaderColumn dataField="edit" dataFormat={this.editFormatter}>Buttons</TableHeaderColumn>
			</BootstrapTable>	
		);
	}
}

BabyPage.propTypes = {
	getBaby: React.PropTypes.func.isRequired,
	deleteBaby: React.PropTypes.func.isRequired
}

export default connect(null, {getBaby, deleteBaby})(BabyPage);
