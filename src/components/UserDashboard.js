import React from 'react';

function UserDashboard (data) {
	return (
		<div className="row">
            <div className="col-md-4 col-md-offset-1 bg-primary box-shadow">
                <h5>Data Bayi</h5>
                <h6>Total Bayi</h6>
                <h3>{data.data.babyCount}</h3>
            </div>
            <div className="col-md-4 col-md-offset-1 bg-primary">
                <h5>Data Ibu Hamil</h5>
                <h6>Total Ibu Hamil</h6>
                <h3>{data.data.expectantCount}</h3>
            </div>
        </div>
	)
}

UserDashboard.propTypes = {
	data: React.PropTypes.object.isRequired
}

export default UserDashboard;
