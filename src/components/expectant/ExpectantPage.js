import React from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn, InsertButton, DeleteButton}  from 'react-bootstrap-table';


import { getExpectant, deleteExpectant } from '../../actions/expectantActions';
class ExpectantPage extends React.Component {
	constructor(props){
        super(props);
        this.state = {
            expectant: []
        }
    }

	componentDidMount(){
		this.props.getExpectant().then(
			(data) => {
                if(data.response === "success"){
                	const expectant = data.result.data;
                	for (var i = 0; i < expectant.length; i++) {
					  expectant[i].edit = expectant[i].id;
					  expectant[i].address = ((expectant[i].address) ? expectant[i].address : "") + ((expectant[i].rt) ? " RT " + expectant[i].rt : "") + ((expectant[i].rw) ? " RW " + expectant[i].rw : "");
					}
                    this.setState({expectant: expectant});
                }
            }
		);
	}

	editFormatter(cell, row) {
	  return `<a href='expectant/edit/${cell}' class='btn btn-warning'>edit</a> `;
	}

	handleInsertButtonClick = (onClick) => {
	  // Custom your onClick event here,
	  // it's not necessary to implement this function if you have no any process before onClick
	  browserHistory.push('/expectant/add');
	}

	createCustomInsertButton = (onClick) => {
	  return (
	    <InsertButton
	      btnText='Add'
	      btnContextual='btn-warning'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-edit'
	      onClick={ () => this.handleInsertButtonClick(onClick) }/>
	  );
	}

	createCustomDeleteButton = (onClick) => {
	  return (
	    <DeleteButton
	      btnText='Delete'
	      btnContextual='btn-danger'
	      className='my-custom-class'
	      btnGlyphicon='glyphicon-trash'/>
	  );
	}

	handleDeleteRow(row) {
		this.props.deleteExpectant(row).then(
			(data) => {
				this.componentDidMount();			
			}
		);
	}

	render(){
		const expectant = this.state.expectant;
		const options = {
			insertBtn: this.createCustomInsertButton,
			onDeleteRow: this.handleDeleteRow.bind(this),
			deleteBtn: this.createCustomDeleteButton
		};
		const selectRow = {
		    mode: 'radio'
		}

		const remote = (remoteFunction) => {
		    // remoteFunction is a object, default is all false, 
		    //  { insertRow: false, dropRow: false, cellEdit: false, sort: false, pagination: false, search: false, filter: false    }
		    //
		    remoteFunction.insertRow = true;
		    remoteFunction.dropRow = true;
		    remoteFunction.cellEdit = true;
		    return remoteFunction;
		}

		return(
			<BootstrapTable data={expectant} options={options} remote={ remote } deleteRow={ true } selectRow= { selectRow } striped hover search multiColumnSearch pagination insertRow>
				<TableHeaderColumn isKey dataField='id'>Id</TableHeaderColumn>
				<TableHeaderColumn dataField='nik'>Nik</TableHeaderColumn>
				<TableHeaderColumn dataField='name'>Nama</TableHeaderColumn>
				<TableHeaderColumn dataField='husband_name'>Nama Suami</TableHeaderColumn>
				<TableHeaderColumn dataField='husband_nik'>Nik Suami</TableHeaderColumn>
				<TableHeaderColumn dataField='dob'>Tanggal Lahir</TableHeaderColumn>
				<TableHeaderColumn dataField='weight_before_pregnant'>Berat Sebelum Hamil</TableHeaderColumn>
				<TableHeaderColumn dataField='mobile_phone'>Nomor Telepon</TableHeaderColumn>
				<TableHeaderColumn dataField='address'>Alamat</TableHeaderColumn>
				<TableHeaderColumn dataField="edit" dataFormat={this.editFormatter}>Buttons</TableHeaderColumn>
			</BootstrapTable>	
		);
	}
}

ExpectantPage.propTypes = {
	getExpectant: React.PropTypes.func.isRequired,
	deleteExpectant: React.PropTypes.func.isRequired
}

export default connect(null, {getExpectant, deleteExpectant})(ExpectantPage);
