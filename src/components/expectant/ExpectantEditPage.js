import React from 'react';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import moment from 'moment';
 
import 'react-datepicker/dist/react-datepicker.css';

import { getExpectantDetail, editExpectant } from '../../actions/expectantActions';

class ExpectantEditPage extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
            posyandus_id: localStorage.posyandus_id,
            nik: '',
            name: '',
            husband_name: '',
            husband_nik: '',
            weight_before_pregnant: '',
            birth_weight: '',
            date: '',
            dob: '',
            mobile_phone: '',
            address: '',
            rt: '',
            rw: '',
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    onChange(e){
        this.setState( {[e.target.name]: e.target.value });
    }

    onSubmit(e){
        this.setState({ errors: {}, isLoading: true });
        e.preventDefault();
        const param = this.state;
        const date = param.date;
        delete param.errors;
        delete param.isLoading;
        delete param.date;
        param.dob = (date !== '') ? date.format("YYYY-MM-DD") : null;
        this.props.editExpectant(param, this.props.params.id).then(
            (data) => {
                if(data.response === "success"){
                    alert(data.message);
                    browserHistory.push('/expectant');
                }
                else{
                    this.setState({date: date});
                    if(data.hasOwnProperty('result') && data.result.hasOwnProperty("error")){
                        const error = data.result.error;
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                    else{
                        const error = {};
                        error.form = data.message;
                        this.setState({errors: error, isLoading:false});
                    }
                }
            }
        );
    }

    handleSelect(date) {
        this.setState({
            date: date
        });
    }

    componentDidMount(){
        this.props.getExpectantDetail(this.props.params.id).then(
            (data) => {
                if(data.response === "success"){
                    const expectant = data.result.data;
                    this.setState( {
                        posyandus_id:  expectant.posyandus_id,
                        nik:  expectant.nik,
                        name:  expectant.name,
                        husband_name: expectant.husband_name,
                        husband_nik: expectant.husband_nik,
                        weight_before_pregnant: expectant.weight_before_pregnant,
                        date:  (expectant.dob) ? moment(expectant.dob, "YYYY-MM-DD"):'',
                        mobile_phone: expectant.mobile_phone,
                        address:  expectant.address,
                        rt:  expectant.rt,
                        rw:  expectant.rw,
                     });
                 }
            }
        );      
    }

    render(){
        const {errors, isLoading} = this.state;
        
        return(
            <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <form onSubmit={this.onSubmit} >
                        { errors.form && <div className="alert alert-danger">{errors.form}</div> }
                        <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
                            <label className="control-label">Nik</label>
                            <input
                                value={this.state.nik || ''}
                                onChange={this.onChange}
                                type="text"
                                name="nik"
                                className="form-control"
                            />
                            {errors.nik && <span className="help-block">{errors.nik}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.name })}>
                            <label className="control-label">Nama</label>
                            <input
                                value={this.state.name || ''}
                                onChange={this.onChange}
                                type="text"
                                name="name"
                                className="form-control"
                            />
                            {errors.name && <span className="help-block">{errors.name}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.husband_name })}>
                            <label className="control-label">Nama Suami</label>
                            <input
                                value={this.state.husband_name || ''}
                                onChange={this.onChange}
                                type="text"
                                name="husband_name"
                                className="form-control"
                            />
                            {errors.husband_name && <span className="help-block">{errors.husband_name}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.husband_nik })}>
                            <label className="control-label">Nik Suami</label>
                            <input
                                value={this.state.husband_nik || ''}
                                onChange={this.onChange}
                                type="text"
                                name="husband_nik"
                                className="form-control"
                            />
                            {errors.husband_nik && <span className="help-block">{errors.husband_nik}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.weight_before_pregnant })}>
                            <label className="control-label">Berat Sebelum Hamil</label>
                            <input
                                value={this.state.weight_before_pregnant || ''}
                                onChange={this.onChange}
                                type="text"
                                name="weight_before_pregnant"
                                className="form-control"
                            />
                            {errors.weight_before_pregnant && <span className="help-weight_before_pregnant">{errors.child_no}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.dob })}>
                            <label className="control-label">Tanggal Lahir</label>
                            <div className="form-control">
                                <DatePicker showYearDropdown selected={this.state.date} onChange={this.handleSelect} />
                            </div>
                            
                            {errors.dob && <span className="help-block">{errors.dob}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.mobile_phone })}>
                            <label className="control-label">No Telepon</label>
                            <input
                                value={this.state.mobile_phone || ''}
                                onChange={this.onChange}
                                type="text"
                                name="mobile_phone"
                                className="form-control"
                            />
                            {errors.mobile_phone && <span className="help-block">{errors.mobile_phone}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.address })}>
                            <label className="control-label">Alamat</label>
                            <textarea 
                                value={this.state.address || ''}
                                onChange={this.onChange}
                                className="form-control" 
                                name="address"
                                rows="3">
                            </textarea>
                            {errors.address && <span className="help-block">{errors.address}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.rt })}>
                            <label className="control-label">Rt</label>
                            <input
                                value={this.state.rt || ''}
                                onChange={this.onChange}
                                type="text"
                                name="rt"
                                className="form-control"
                            />
                            {errors.rt && <span className="help-block">{errors.rt}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.rw })}>
                            <label className="control-label">Rw</label>
                            <input
                                value={this.state.rw || ''}
                                onChange={this.onChange}
                                type="text"
                                name="rw"
                                className="form-control"
                            />
                            {errors.rw && <span className="help-block">{errors.rw}</span>}
                        </div>
                        <div className={classnames('form-group', { 'has-error': errors.phone_number })}>
                            <button className="btn btn-primary btn-lg col-md-12" disabled={isLoading}>
                                Save!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
                    
        );
    }
}

ExpectantEditPage.propTypes = {
    getExpectantDetail: React.PropTypes.func.isRequired,
    editExpectant: React.PropTypes.func.isRequired
}

export default connect(null, {getExpectantDetail, editExpectant})(ExpectantEditPage);
