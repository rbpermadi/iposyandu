import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import { logout } from '../actions/authActions';

class NavigationBar extends React.Component{
	logout(e){
		e.preventDefault();
		this.props.logout();
	}
	render(){
		const { isAuthenticated } = this.props.auth;

		const GuestLink = (
			<ul className="nav navbar-nav navbar-right">
				<li className="active"><Link to="/signup">Daftar</Link></li>
				<li className="active"><Link to="/signin">Masuk</Link></li>
			</ul>
		);

		const UserLink = (
			<ul className="nav navbar-nav navbar-right">
				<li className="active"><Link to="/checkup">Check Up</Link></li>
				<li className="active"><Link to="/expectant">Ibu Hamil</Link></li>
				<li className="active"><Link to="/baby">Bayi</Link></li>
				<li className="active"><a href="/logout" onClick={this.logout.bind(this)}>Logout</a></li>
			</ul>
		);

		return (
			<nav className="navbar navbar-inverse">
				<div className="container-fluid">
					<div className="navbar-header">
						<Link to="/" className="navbar-brand">Iposyandu</Link>
					</div>
					<div className="collapse navbar-collapse">
						{isAuthenticated ? UserLink : GuestLink}
					</div>
				</div>
			</nav>
		);
	}
}

NavigationBar.propTypes = {
	auth: React.PropTypes.object.isRequired,
	logout: React.PropTypes.func.isRequired
}

function mapStateToProps(state){
	return {
		auth: state.auth
	};
}

export default connect(mapStateToProps, { logout })(NavigationBar);

