import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import Dashboard from './components/Dashboard';
import SignUpPage from './components/signup/SignUpPage';
import SignInPage from './components/signin/SignInPage';
import SignOutPage from './components/signin/SignOutPage';
import BabyPage from './components/baby/BabyPage';
import BabyAddPage from './components/baby/BabyAddPage';
import BabyEditPage from './components/baby/BabyEditPage';
import ExpectantPage from './components/expectant/ExpectantPage';
import ExpectantAddPage from './components/expectant/ExpectantAddPage';
import ExpectantEditPage from './components/expectant/ExpectantEditPage';
import CheckupMainPage from './components/checkup/CheckupMainPage';
import CheckupPage from './components/checkup/CheckupPage';
import BabyCheckupPage from './components/checkup/BabyCheckupPage';
import BabyCheckupAddPage from './components/checkup/BabyCheckupAddPage';
import BabyCheckupEditPage from './components/checkup/BabyCheckupEditPage';
import ExpectantCheckupPage from './components/checkup/ExpectantCheckupPage';
import ExpectantCheckupAddPage from './components/checkup/ExpectantCheckupAddPage';
import ExpectantCheckupEditPage from './components/checkup/ExpectantCheckupEditPage';

export default (
	<Route path="/"  component={App}>
		<IndexRoute component={Dashboard}/>
		<Route path="signup" component={SignUpPage} />
		<Route path="signin" component={SignInPage} />
		<Route path="signout" component={SignOutPage} />
		<Route path="baby" >
			<IndexRoute component={BabyPage} />
			<Route path="add" component={BabyAddPage} />
			<Route path="edit/:id" component={BabyEditPage} />
		</Route>
		<Route path="expectant" >
			<IndexRoute component={ExpectantPage} />
			<Route path="add" component={ExpectantAddPage} />
			<Route path="edit/:id" component={ExpectantEditPage} />
		</Route>
		<Route path="checkup" component={CheckupMainPage}>
			<IndexRoute component={CheckupPage} />
			<Route path="expectant">
				<IndexRoute component={ExpectantCheckupPage}/>
				<Route path="add" component={ExpectantCheckupAddPage}/>
				<Route path="edit/:id" component={ExpectantCheckupEditPage} />
			</Route>
			<Route path="baby">
				<IndexRoute component={BabyCheckupPage}/>
				<Route path="add" component={BabyCheckupAddPage}/>
				<Route path="edit/:id" component={BabyCheckupEditPage} />
			</Route>
		</Route>
	</Route>
)
