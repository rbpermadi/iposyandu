import axios from 'axios';

const api_url = 'http://apibebas.gntl.work/public/api/';

export function get (uri){
	var encodedURI = window.encodeURI(api_url+uri);
	
	return axios.get(encodedURI);
}

export function post (uri, data){
	var encodedURI = window.encodeURI(api_url+uri);

	return axios.post(encodedURI, data);
}

export function put (uri, data){
	var encodedURI = window.encodeURI(api_url+uri);

	return axios.put(encodedURI, data);
}

export function deleteData (uri){
	var encodedURI = window.encodeURI(api_url+uri);

	return axios.delete(encodedURI);
}