import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput (data){
	let errors = {};

    if (Validator.isNull(data.public_healths_id)) {
        errors.public_healths_id = 'This field is required';
    }
    if (Validator.isNull(data.posyandus_id)) {
        errors.posyandus_id = 'This field is required';
    }
    if (Validator.isNull(data.phone_number)) {
        errors.phone_number = 'This field is required';
    }
    if (Validator.isNull(data.name)) {
        errors.name = 'This field is required';
    }
    if (Validator.isNull(data.password)) {
        errors.password = 'This field is required';
    }
    if (Validator.isNull(data.password_confirmation)) {
        errors.password_confirmation = 'This field is required';
    }
    if (!Validator.equals(data.password, data.password_confirmation)) {
        errors.password_confirmation = 'Passwords must match';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}