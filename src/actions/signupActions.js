import * as api from '../utils/api';

export function userSignupRequest (data){
	return dispatch => {
		return api.post('auth/register', data).then(
			(data) => {
				return data.data;
            },
            (err) => {
                return err.response.data;
            }
		);
	}
}
