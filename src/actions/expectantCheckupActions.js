import * as api from '../utils/api';

export function getExpectantCheckup (){
    return dispatch => {
        return api.get('expectant_checkup?length=all').then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function addExpectantCheckup (data){
    return dispatch => {
        return api.post('expectant_checkup', data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function editExpectantCheckup (data, id){
    return dispatch => {
        return api.put('expectant_checkup/'+id, data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function getExpectantCheckupDetail (id){
    return dispatch => {
        return api.get('expectant_checkup/'+id).then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function deleteExpectantCheckup (id){
    return dispatch => {
        return api.deleteData('expectant_checkup/'+id).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}
