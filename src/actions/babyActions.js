import * as api from '../utils/api';

export function babyCount (){
    return dispatch => {
        return api.get('baby/babyCount').then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function getBaby (){
    return dispatch => {
        return api.get('baby?length=all').then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function addBaby (data){
    return dispatch => {
        return api.post('baby', data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function editBaby (data, id){
    return dispatch => {
        return api.put('baby/'+id, data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function getBabyDetail (id){
    return dispatch => {
        return api.get('baby/'+id).then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function deleteBaby (id){
    return dispatch => {
        return api.deleteData('baby/'+id).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}
