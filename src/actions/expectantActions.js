import * as api from '../utils/api';

export function expectantCount (){
    return dispatch => {
        return api.get('expectant/expectantCount').then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function getExpectant (){
    return dispatch => {
        return api.get('expectant?length=all').then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function addExpectant (data){
    return dispatch => {
        return api.post('expectant', data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function editExpectant (data, id){
    return dispatch => {
        return api.put('expectant/'+id, data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function getExpectantDetail (id){
    return dispatch => {
        return api.get('expectant/'+id).then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function deleteExpectant (id){
    return dispatch => {
        return api.deleteData('expectant/'+id).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}
