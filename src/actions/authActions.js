import {browserHistory} from 'react-router';

import * as api from '../utils/api';
import setAuthorizationToken from '../utils/setAuthorizationToken';
import { SET_CURRENT_USER } from './types';

export function setCurrentUser(user) {
  return {
    type: SET_CURRENT_USER,
    user
  };
}

export function logout() {
  return dispatch => {
    localStorage.removeItem('auth_token');
    localStorage.removeItem('posyandus_id');
    setAuthorizationToken(false);
    dispatch(setCurrentUser({}));
    browserHistory.push('/');
  }
}

export function login (data){
	
	return dispatch => {
		return api.post('auth/login', data).then(
			(data) => {
				if(data.data.response === "success"){
					const token = data.data.result.data.token;
		            localStorage.setItem("auth_token", data.data.result.data.token );
                localStorage.setItem("posyandus_id", data.data.result.data.posyandus_id );
		            setAuthorizationToken(token);
		            dispatch(setCurrentUser({token: token}));
		            return data.data;
				}
                else{
                	return data.data;
                }
            },
            (err) => {
                return err.response.data;
            }
		);
	}
}