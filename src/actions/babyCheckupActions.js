import * as api from '../utils/api';

export function getBabyCheckup (){
    return dispatch => {
        return api.get('baby_checkup?length=all').then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function addBabyCheckup (data){
    return dispatch => {
        return api.post('baby_checkup', data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function editBabyCheckup (data, id){
    return dispatch => {
        return api.put('baby_checkup/'+id, data).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function getBabyCheckupDetail (id){
    return dispatch => {
        return api.get('baby_checkup/'+id).then(
            (data) => {
                return data.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}

export function deleteBabyCheckup (id){
    return dispatch => {
        return api.deleteData('baby_checkup/'+id).then(
            (res) => {
                return res.data;
            },
            (err) => {
                return err.response.data;
            }
        );
    }
}
